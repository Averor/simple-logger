<?php

namespace Averor\SimpleLogger;

use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use ReflectionClass;
use ReflectionException;

/**
 * Class Logger
 *
 * @package Averor\SimpleLogger
 * @author Averor <averor.dev@gmail.com>
 */
class Logger implements LoggerInterface
{
    /** @var resource  */
    protected $target;

    public function __construct($target)
    {
        $this->target = \fopen($target, 'a');
    }

    /**
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void
     * @throws ReflectionException
     */
    public function log($level, $message, array $context = array())
    {
        $reflection = new ReflectionClass(LogLevel::class);

        if (!in_array($level, $reflection->getConstants())) {
            throw new InvalidArgumentException('Unknown log level');
        }
        \fwrite(
            $this->target,
            static::interpolate($level . ' ' . $message, $context) . PHP_EOL
        );
    }

    public function emergency($message, array $context = array())
    {
        $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    public function alert($message, array $context = array())
    {
        $this->log(LogLevel::ALERT, $message, $context);
    }

    public function critical($message, array $context = array())
    {
        $this->log(LogLevel::CRITICAL, $message, $context);
    }

    public function error($message, array $context = array())
    {
        $this->log(LogLevel::ERROR, $message, $context);
    }

    public function warning($message, array $context = array())
    {
        $this->log(LogLevel::WARNING, $message, $context);
    }

    public function notice($message, array $context = array())
    {
        $this->log(LogLevel::NOTICE, $message, $context);
    }

    public function info($message, array $context = array())
    {
        $this->log(LogLevel::INFO, $message, $context);
    }

    public function debug($message, array $context = array())
    {
        $this->log(LogLevel::DEBUG, $message, $context);
    }

    /**
     * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
     */
    protected static function interpolate($message, array $context = array())
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be casted to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }
}
