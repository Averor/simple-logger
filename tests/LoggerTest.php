<?php

namespace Averor\SimpleLogger\Test;

use Averor\SimpleLogger\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\Test\LoggerInterfaceTest;

/**
 * Class LoggerTest
 *
 * @package Averor\SimpleLogger\Test
 * @author Averor <averor.dev@gmail.com>
 */
class LoggerTest extends LoggerInterfaceTest
{
    protected static $bufferExcludedFor = [
        'testImplements',
        'testThrowsOnInvalidLevel'
    ];

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return new Logger('php://output');
    }

    protected function setUp()
    {
        if (!in_array($this->getName(), static::$bufferExcludedFor)) {
            ob_start();
        }
    }

    /**
     * This must return the log messages in order.
     *
     * The simple formatting of the messages is: "<LOG LEVEL> <MESSAGE>".
     *
     * Example ->error('Foo') would yield "error Foo".
     *
     * @return string[]
     */
    public function getLogs()
    {
        return explode(
            PHP_EOL,
            rtrim(
                ob_get_clean(),
                PHP_EOL
            )
        );
    }
}
