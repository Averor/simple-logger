# Averor/Simple-Logger

[![pipeline status](https://gitlab.com/Averor/simple-logger/badges/master/pipeline.svg)](https://gitlab.com/Averor/simple-logger/commits/master)
[![coverage report](https://gitlab.com/Averor/simple-logger/badges/master/coverage.svg)](https://gitlab.com/Averor/simple-logger/commits/master)
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/Averor/simple-logger/raw/master/LICENSE)
[![Semver](http://img.shields.io/SemVer/1.0.0.png)](http://semver.org/spec/v1.0.0.html)

Simplest possible (?) implementation of \Psr\Log\LoggerInterface.

Usable for unit/functional testing of logger-involved application parts.

 

## Usage

### Examples
`$logger = new \Averor\SimpleLogger\Logger('php://output');`

`$logger = new \Averor\SimpleLogger\Logger('php://stdout');`

`$logger = new \Averor\SimpleLogger\Logger('/tmp/some.log');`

## Testing with PHPUnit
`$ ./vendor/bin/phpunit ./tests`
 